# **HOROSCOPES API Ver 1.0**
``HOST: http://api.domain.com/api/v1``

## **Authorizations**
``Base URL: /auth``

### Login
- URI: **/login**
- Method: **POST**
- Header:
	+ Content-Type: application/json
- Request:
```
{
	"email": "peter@gmail.com",
	"password": "******"
}
```
- Response:
	+ status: **200** *(OK)*, **400** *(Bad request)*, **401** *(Unauthorize)*
	+ body: ```{"token": "this-is-jwt-token"}```

### Register
- URI: **/register**
- Method: **POST**
- Header:
	+ Content-Type: application/json
- Request:
```
{
	"dob": "1991-12-30",
	"tob": "15:00", //or NULL
	"timezone": "+7",
	"gender": "male",
	"name": "Peter Nguyen",
	"email": "peter@gmail.com",
	"password": "******",
}
```

### Register by Facebook
- URI: **/fb-register**
- Method: **POST**
- Header:
	+ Content-Type: application/json
- Request:
```
{
	"dob": "1991-12-30",
	"tob": "15:00", //or NULL
	"timezone": "+7",
	"email": "peter@gmail.com",
	"access_token": "this-is-facebook-access-token",
}
```
- Response:
	+ status: **200** *(OK)*, **400** *(Bad request)*, **401** *(Unauthorize)*
	+ body: ```{"token": "this-is-jwt-token"}```

### Logout
- URI: **/logout**
- Method: **GET**
- Header:
	+ Authorization: Token token="this-is-jwt-token"
- Response:
	+ status **200** *(OK)*, **401** *(Unauthorize)*

### Forgot password
- URI: **/forgot**
- Method: **POST**
- Header:
	+ Content-Type: application/json
- Request:
```{ "email": "alex@email.com" }```
- Response:
	+ status **200** *(OK)*, **400** *(Bad request)*, **404** *(Not found)*

### Verify token
- URI: **/verify-token**
- Method: **POST**
- Header:
	+ Content-Type: application/json
- Request:
```
{ "token": "this-is-jwt-token", "email": "peter@email.com" }
```
- Response:
	+ status: **200**, **400**


## **Users**
``Base URL: /users``

### Get profile
- URI: **/**
- Method: **GET**
- Header:
	+ Content-Type: application/json
	+ Authorization: Token token="this-is-jwt-token"
- Response:
	+ status **200** *(OK)*, **401** *(Unauthorize)*, **404** *(Not found)*
	+ body: *for status 200 only*
```
{
	"name": "Peter Nguyen",
	"email": "peter@gmail.com",
	"dob": "2015-12-30",
	"tob": "17:58",
	"timezone": "+7",
	"horoscopes": "scorpio",
	"avatar": "http://cdn.domain.com/path/to/avatar/peter-nguyen.jpg"
}
```

### Update profile
- URI: **/**
- Method: **PUT**
- Header:
	+ Content-Type: application/json
	+ Authorization: Token token="this-is-jwt-token"
- Request:
```
{
	"name": "New name",
	"dob": "2015-12-30",
	"tob": "17:58",
	"timezone": "+7"
}
```
- Response:
	+ status **201** *(OK)*, **401** *(Unauthorize)*, **400** *(Bad request)*,  **404** *(Not found)*

### Update avatar
- URI: **/avatar**
- Method: **POST** *(for upload)* , **PUT** *(for update)*
- Header:
	+ Content-Type: **multipart/form-data**
	+ Authorization: Token token="this-is-jwt-token"
- Request: ```avatar:file```
- Response:
	+ status **201** *(OK)*, **401** *(Unauthorize)*, **400** *(Bad request)*,  **404** *(Not found)*

## **Horoscopes**
``Base URL: /horoscopes``

### Intro
- URI: **/intro**
- Method: **POST**
- Header:
	+ Content-Type: application/json
- Request:
```
{
	"dob": "YYYY-MM-DD",
	"tob": "HH:mm", //or NULL
	"timezone": "+7"
}
```
- Response:
	+ status: **200**, **400**
	+ body:  for status 200 only
```
{
	"name": "cancer",
	"description": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'"
}
```

### Daily
- URI: **/daily**
- Method: **GET**
- Header:
	+ Content-Type: application/json
	+ Authorization: Token token="this-is-jwt-token"
- Response:
	+ status: **200**, **400**,  **401**
	+ body:  for status 200 only
```
{
	"name": "cancer",
	"today": {
        "content": "Day la content",
        "star": 4
    },
    "yesterday": {
        "content": "Day la content",
        "star": 3
    },
    "tomorrow": {
        "content": "Day la content",
        "star": 3
    }
}
```

### Weekly
- URI: **/weekly**
- Method: **GET**
- Header:
	+ Content-Type: application/json
	+ Authorization: Token token="this-is-jwt-token"
- Response:
	+ status: **200**, **400**,  **401**
	+ body:  for status 200 only
```
{
	"name": "cancer",
	"from": "YYYY-MM-DD",
	"to": "YYYY-MM-DD",
	"start": 4.5,
	"content": "<h3>This week</h3> <p>html content</p>..."
}
```

### Yearly
- URI: **/yearly**
- Method: **GET**
- Header:
	+ Content-Type: application/json
	+ Authorization: Token token="this-is-jwt-token"
- Response:
	+ status: **200**, **400**,  **401**
	+ body:  for status 200 only
```
{
	"name": "cancer",
	"start": 4.5,
	"content": "<h3>Year 2015</h3> <p>html content</p>..."
}
```

### Monthly
- URI: **/monthly**
- Method: **GET**
- Header:
	+ Content-Type: application/json
	+ Authorization: Token token="this-is-jwt-token"
- Response:
	+ status: **200**, **400**,  **401**
	+ body:  for status 200 only
```
{
	"name": "cancer",
	"start": 4.5,
	"content": "<h3>Month 02</h3> <p>html content</p>..."
}
```

### Compatibility
- URI: **/compatibility**
- Method: **GET**
- Header:
	+ Content-Type: application/json
	+ Authorization: Token token="this-is-jwt-token"
- Request:
	+ your_sign: your sign *(ex: `cancer`)*
	+ your_gender: `male` / `female`
	+ partner_sign: partner's sign  *(ex: `aries`)*
- Response:
	+ status: **200**, **400**,  **401**
	+ body:  for status 200 only
```
{
	"your_sign": "cancer",
	"partner_sign": "aries",
	"star": 2.5,
	"description": "Challenging",
	"content": "<h3>Cancer and Aries</h3> <p>html content</p>..."
}
```

###  Horoscope profile
- URI: **/profile**
- Method: **GET**
- Header:
	+ Content-Type: application/json
	+ Authorization: Token token="this-is-jwt-token"
- Response:
	+ status: **200**, **400**,  **401**
	+ body:  for status 200 only
```
{
	"name": "cancer",
	"description": "Lorem ipsum..."
}
```

## **Image Services** *(image service)*
``HOST: http://img.domain.com``

### Horoscopes
- URI: **/horoscope/(:horoscope-name)_(:version)**
- Method: **GET**
- Params:
	+ horoscope-name: Horoscope's name *(ex: cancer)*
	+ version: Image version - size *(ex: s, m, l)*
- Response: Horoscope image file, if empty return `no_image`
![cancer](http://www.webtretho.com/wttnews/wp-content/uploads/2012/12/wtt-cu-giai.jpg)