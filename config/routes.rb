require 'sidekiq/web'
require 'sidetiq/web'

Rails.application.routes.draw do
  
  devise_for :admin
  
  authenticate :admin do
    mount Sidekiq::Web => '/sidekiq'
  end

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  get '/horoscope/:name' => 'api/v1/horo#getSign'

  namespace :api, defaults: { format: :json} do
    namespace :v1 do
      # root
      get '/' => 'index#index'

      # auth  
      scope '/auth' do
        get '/logout'           => 'auth#logout'
        post '/login'           => 'auth#login'
        post '/register'        => 'auth#register'
        post '/fb-register'     => 'auth#loginFB'
        post '/forgot'          => 'auth#resetPassword'
        post '/verify-token'    => 'auth#verifyToken'
      end

      # horoscopes
      scope '/horoscopes' do 
        post '/intro'           => 'horo#getIntro'
        get  '/profile'         => 'horo#getProfile'
        get  '/daily'           => 'horo#daily'
        get  '/weekly'          => 'horo#weekly'
        get  '/yearly'          => 'horo#yearly'
        get  '/monthly'         => 'horo#monthly'
        get  '/compatibility'   => 'horo#compare'
      end

      # user
      scope '/users' do 
        get '/'                 => 'user#getProfile'
        put '/'                 => 'user#update'
        put '/avatar'           => 'user#uploadAvatar'
        post '/avatar'          => 'user#uploadAvatar'
      end

    end
  end
end
