RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :admin
  end
  config.current_user_method(&:current_admin)

  # config.included_models = ['HoroProfile','HoroProfile::Translation']
  config.included_models = config.models_pool + %W(Compatibility::Translation HoroProfile::Translation HoroDailyEntry::Translation
                                                   Horo::Translation HoroWeeklyEntry::Translation HoroMonthlyEntry::Translation HoroYearlyEntry::Translation)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    # export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'HoroProfile' do
    configure :translations, :globalize_tabs
    label 'Horoscopes Profile'
    show do
      field :horo 
      field :gender 
      field :description 
      field :content
    end
    list do
      field :horo
      field :gender
      field :description
      field :content
    end
    create do
      field :horo
      field :gender, :enum do
        help 'Please select an option below'
        enum do
          ['male', 'female']
        end
      end
      field :translations
    end
    edit do
      field :horo
      field :gender, :enum do
        help 'Please select an option below'
        enum do
          ['male', 'female']
        end
      end
      field :translations
    end
  end

  config.model 'HoroProfile::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :description, :content
  end

  config.model 'Compatibility' do
    configure :translations, :globalize_tabs
    show do
      field :horo_me
      field :horo_partner
      field :gender
      field :star
      field :description
      field :content
    end
    list do
      field :horo_me
      field :horo_partner
      field :gender
      field :star
      field :description
      field :content
    end
    create do
      field :horo_me
      field :horo_partner
      field :gender, :enum do
        help 'Please select an option below'
        enum do
          ['male', 'female']
        end
      end
      field :star
      field :translations
    end
    edit do
      field :horo_me
      field :horo_partner
      field :gender, :enum do
        help 'Please select an option below'
        enum do
          ['male', 'female']
        end
      end
      field :star
      field :translations
    end
  end

  config.model 'Compatibility::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :description, :content
  end

  config.model 'Admin' do
    visible false
    edit do
      field :email
      field :password
      field :password_confirmation
    end
    create do
      field :email
      field :password
      field :password_confirmation
    end
  end

  config.model 'Horo' do
    configure :translations, :globalize_tabs
    label 'Horoscope'
    list do
      field :name
      field :code
      field :sign
      field :from_date
      field :from_month
      field :to_date
      field :to_month
    end
    show do
      field :name
      field :code
      field :sign
      field :from_date
      field :from_month
      field :to_date
      field :to_month
    end
    edit do
      field :translations
      field :code
      field :sign
      field :from_date
      field :from_month
      field :to_date
      field :to_month
    end
    create do
      field :translations
      field :code
      field :sign
      field :from_date
      field :from_month
      field :to_date
      field :to_month
    end
  end

  config.model 'Horo::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :name
  end


  config.model 'HoroDailyEntry' do
    configure :translations, :globalize_tabs
    label 'Horoscopes Daily Entry'
    list do
      field :horo
      field :date
      field :star
      field :content
    end
    show do
      field :horo
      field :date
      field :star
      field :content
    end
    create do
      field :horo
      field :date
      field :star
      field :translations
    end
    edit do
      field :horo
      field :date
      field :star
      field :translations
    end
  end

  config.model 'HoroDailyEntry::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :content
  end

  config.model 'HoroWeeklyEntry' do
    configure :translations, :globalize_tabs
    label 'Horoscopes Weekly Entry'
    list do
      field :horo
      field :from_date
      field :to_date
      field :star
      field :content
    end
    show do
      field :horo
      field :from_date
      field :to_date
      field :star
      field :content
    end
    create do
      field :horo
      field :from_date
      field :to_date
      field :star
      field :translations
    end
    edit do
      field :horo
      field :from_date
      field :to_date
      field :star
      field :translations
    end
  end

  config.model 'HoroWeeklyEntry::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :content
  end

  config.model 'HoroMonthlyEntry' do
    configure :translations, :globalize_tabs
    label 'Horoscopes Monthly Entry'
    list do
      field :horo
      field :month
      field :star
      field :content
    end
    show do
      field :horo
      field :month
      field :star
      field :content
    end
    create do
      field :horo
      field :month
      field :star
      field :translations
    end
    edit do
      field :horo
      field :month
      field :star
      field :translations
    end
  end

  config.model 'HoroMonthlyEntry::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :content
  end

  config.model 'HoroYearlyEntry' do
    configure :translations, :globalize_tabs
    label 'Horoscopes Yearly Entry'
    list do
      field :horo
      field :year
      field :star
      field :content
    end
    show do
      field :horo
      field :year
      field :star
      field :content
    end
    create do
      field :horo
      field :year
      field :star
      field :translations
    end
    edit do
      field :horo
      field :year
      field :star
      field :translations
    end
  end

  config.model 'HoroYearlyEntry::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :content
  end

  config.model 'User' do
    list do
      field :name
      field :email
      field :dob
      field :horo
      field :tob
      field :timezone
      field :gender
      field :avatar
    end
    create do
      field :name
      field :email
      field :dob
      field :tob
      field :password
      field :avatar
      field :timezone, :enum do
        help 'Please select an option below'
        enum do
          ['+1:00', '+2:00', '+2:30', '+3:00', '+3:30', '+4:00', '+5:00', '+5:30', '+6:00', '+7:00', '+8:00', '+9:00',
           '+9:30', '+10:00', '+11:00', '+12:00', '-11:00', '-10:00', '-9:00', '-8:00', '-7:00', '-6:00', '-5:00', '-4:00', '-3:00', '-1:00']
        end
      end
      field :gender, :enum do
        help 'Please select an option below'
        enum do
          ['male', 'female']
        end
      end
    end
    edit do
      field :name
      field :email
      field :dob
      field :horo
      field :tob
      field :timezone, :enum do
        help 'Please select an option below'
        enum do
          ['+1:00', '+2:00', '+2:30', '+3:00', '+3:30', '+4:00', '+5:00', '+5:30', '+6:00', '+7:00', '+8:00', '+9:00',
           '+9:30', '+10:00', '+11:00', '+12:00', '-11:00', '-10:00', '-9:00', '-8:00', '-7:00', '-6:00', '-5:00', '-4:00', '-3:00', '-1:00']
        end
      end
      field :password
      field :avatar
      field :gender, :enum do
        help 'Please select an option below'
        enum do
          ['male', 'female']
        end
      end
    end
  end

end
