class FindHoroscopeJob < ActiveJob::Base
  include Api::V1::Findhoroscope
  
  queue_as :default

  def perform(user)
    find(user.dob)
    user.update_attributes horo_id: @horo.id
  end
  
end