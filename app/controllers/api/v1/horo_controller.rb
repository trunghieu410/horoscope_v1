class Api::V1::HoroController < Api::V1::ApplicationController
  include Api::V1::Authorize
  include Api::V1::Findhoroscope

  before_action :setLanguage
  before_action :authenticate,  except: [:getIntro, :getSign]
  before_action :checkUser, only: [:daily, :weekly, :yearly, :monthly]

  def getIntro
    begin
      find(params['dob'])
      @horoProfile = HoroProfile.find_by_horo_id(@horo.id)
    rescue
      return head 400
    end
  end

  def getProfile
    begin
      @horo = Horo.find(@user.horo_id)
      @horoProfile = Rails.cache.fetch("HoroProfile-#{@user.id}") do
        HoroProfile.find_by_horo_id(@user.horo_id)
      end
    rescue
      return head 400
    end
  end

  def daily
    @yesterday = Rails.cache.fetch("Daily-#{@user.horo_id}-#{Date.today-1}", :expires_in => 1.day) do
      HoroDailyEntry.where(:date => Date.today-1, :horo_id => @user.horo_id).first
    end
    @today     = Rails.cache.fetch("Daily-#{@user.horo_id}-#{Date.today}", :expires_in => 2.day) do
      HoroDailyEntry.where(:date => Date.today, :horo_id => @user.horo_id).first
    end
    @tomorrow  = Rails.cache.fetch("Daily-#{@user.horo_id}-#{Date.today+1}", :expires_in => 3.day) do
      HoroDailyEntry.where(:date => Date.today+1, :horo_id => @user.horo_id).first
    end
    return head 404 if @yesterday.nil? && @today.nil? && @tomorrow.nil?
  end

  def weekly
    week_end      = Date.today.at_end_of_week
    week_begining = Date.today.at_beginning_of_week

    @weekly = Rails.cache.fetch("Weekly-#{@user.horo_id}-#{week_begining}", :expires_in => 7.day) do
      HoroWeeklyEntry.where(:horo_id => @user.horo_id, :from_date => week_begining, :to_date => week_end).first
    end
    return head 404 if @weekly.nil? 
  end

  def yearly
    @yearly = Rails.cache.fetch("Yearly-#{@user.horo_id}-#{Time.now.year}", :expires_in => 1.year) do
      HoroYearlyEntry.where(:horo_id => @user.horo_id, :year => Time.now.year).first
    end
    return head 404 if @yearly.nil? 
  end

  def monthly
    @monthly = Rails.cache.fetch("Monthly-#{@user.horo_id}-#{Time.now.month}", :expires_in => 1.month) do
      HoroMonthlyEntry.where(:horo_id => @user.horo_id, :month => Time.now.month).first
    end
    return head 404 if @monthly.nil? 
  end

  def compare
    if @user.present?
      if params["your_gender"] == "male"
        first_horo  = Horo.find_by_name(params["your_sign"])
        second_horo = Horo.find_by_name(params["partner_sign"])
      else
        first_horo  = Horo.find_by_name(params["partner_sign"])
        second_horo = Horo.find_by_name(params["your_sign"])
      end
      @result = Rails.cache.fetch("Compare-#{first_horo}-#{second_horo}-#{params["your_gender"]}") do
        Compatibility.where(:first_horo => first_horo, :second_horo => second_horo).first
      end
      return head 404 if @result.nil?
    else
      return head 401
    end
   end

  def getSign
    horo = Horo.find_by_code(params[:name])
    if !horo.nil? && !horo.sign.url.nil?
      img_url = 'public' + horo.sign.url
      if FileTest.exist?(img_url)
        send_file img_url, type: 'image/png', disposition: 'inline'
      else
        send_file 'public/default/no_image_available.png', type: 'image/png', disposition: 'inline'
      end
    else
      send_file 'public/default/no_image_available.png', type: 'image/png', disposition: 'inline'
    end
  end

  private
  def checkUser
    if @user.present?
      @horo = Horo.find(@user.horo_id)
    else
      return head 401
    end
  end

  def setLanguage
    I18n.locale = :en
  end

end