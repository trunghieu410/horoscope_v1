module Api::V1::Findhoroscope extend ActiveSupport::Concern

  def find(dob)
    @horoname = %w(Capricorn Aquarius Pisces Aries Taurus Gemini Cancer Leo Virgo Libra Scorpio Sagittarius)
    day     = dob.to_date.day.to_i
    month   = dob.to_date.month.to_i
    max_day = day
    caculateHoro(day, month)
  end

  private
  def caculateHoro(day, month)
    name =''
    max_day = Horo.find_by_to_month(month).to_date

    if day <= max_day 
      name = @horoname.at(month -1)
      @horo = Horo.find_by_name(name)
    else
      month += 1
      month = 1 if month == 13
      caculateHoro(1, month)
    end
  end

end