class CrawlDailyWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence { daily(1).hour_of_day(0) }

  def perform
    link      = "http://www.astrology.com/horoscope/daily/" 
    list_day  = %w[today tomorrow]
    list_horo = %w[aries taurus gemini cancer leo virgo libra scorpio sagittarius capricorn aquarius pisces]

    list_day.each do |day|
      url = link + day
      day == "today" ? horo_day = Date.today : horo_day = Date.today + 1

      list_horo.each do |horo|
        crawl_url = url + "/" + horo + ".html"

        data = Nokogiri::HTML(open(crawl_url)).css("div.page-horoscope-text")
        content = data.text

        horo_id = Horo.find_by_code(horo).id
        old_horo = HoroDailyEntry.where(:horo_id => horo_id, :date => horo_day)
        new_horo = HoroDailyEntry.new(:horo_id   => horo_id, 
                                      :date      => horo_day, 
                                      :content   => content)
        new_horo.save if old_horo.blank?
      end
    end
  end
  
end