json.name 				@horo.name
json.yesterday do
	json.content		@yesterday.nil? ? "No data for yesterday" : @yesterday.content
	json.star 			@yesterday.nil? ? 0 : @yesterday.star
end
json.today do
	json.content		@today.nil? ? "No data for today" : @today.content
	json.star 			@today.nil? ? 0 : @today.star
end
json.tomorrow do
	json.content		@tomorrow.nil? ? "No data for tomorrow" : @tomorrow.content
	json.star 			@tomorrow.nil? ? 0 : @tomorrow.star
end


