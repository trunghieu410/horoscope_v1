class HoroProfile < ActiveRecord::Base
	include Clearcache

  belongs_to :horo
  translates :description, :content
  accepts_nested_attributes_for :translations, allow_destroy: true
  after_update { deleteCache('HoroProfile/#{self.id}') }

end
