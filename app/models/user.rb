class User < ActiveRecord::Base
  belongs_to :horo
  
  has_secure_password
  mount_uploader :avatar, AvatarUploader 
  after_create { findhoros }

  validate :dob_cannot_be_in_the_future
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}

  private
  def findhoros
    FindHoroscopeJob.perform_later(self)
  end

  def dob_cannot_be_in_the_future
    if dob.present? && dob >= Date.today
      errors.add(:dob, "can't be in the future")
    end
  end

end