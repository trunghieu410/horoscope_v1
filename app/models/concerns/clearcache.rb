module Clearcache extend ActiveSupport::Concern
  def deleteCache(key)
    Rails.cache.delete(key)
  end
end