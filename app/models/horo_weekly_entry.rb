class HoroWeeklyEntry < ActiveRecord::Base
	include Clearcache

  belongs_to :horo
  translates :content
  accepts_nested_attributes_for :translations, allow_destroy: true
  after_update { deleteCache('Weekly-#{self.horo_id}-#{self.from_date}')}

end
