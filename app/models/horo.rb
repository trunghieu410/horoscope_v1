class Horo < ActiveRecord::Base
	translates :name
	accepts_nested_attributes_for :translations, allow_destroy: true
  mount_uploader :sign, AvatarUploader 
  
end
