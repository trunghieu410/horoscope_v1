class Compatibility < ActiveRecord::Base
  include Clearcache

	belongs_to :horo_me, :class_name => 'Horo', :foreign_key => 'first_horo'
	belongs_to :horo_partner, :class_name => 'Horo', :foreign_key => 'second_horo'
	
  translates :description, :content
  accepts_nested_attributes_for :translations, allow_destroy: true

  after_update { deleteCache('Compare-#{self.first_horo}-#{self.second_horo}-#{self.gender}') }

end
