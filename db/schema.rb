# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150831084521) do

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "compatibilities", force: :cascade do |t|
    t.integer  "first_horo",  limit: 4
    t.integer  "second_horo", limit: 4
    t.string   "gender",      limit: 255
    t.float    "star",        limit: 24
    t.text     "description", limit: 65535
    t.text     "content",     limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "compatibilities", ["first_horo"], name: "index_compatibilities_on_first_horo", using: :btree
  add_index "compatibilities", ["second_horo"], name: "index_compatibilities_on_second_horo", using: :btree

  create_table "compatibility_translations", force: :cascade do |t|
    t.integer  "compatibility_id", limit: 4,     null: false
    t.string   "locale",           limit: 255,   null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.text     "description",      limit: 65535
    t.text     "content",          limit: 65535
  end

  add_index "compatibility_translations", ["compatibility_id"], name: "index_compatibility_translations_on_compatibility_id", using: :btree
  add_index "compatibility_translations", ["locale"], name: "index_compatibility_translations_on_locale", using: :btree

  create_table "horo_daily_entries", force: :cascade do |t|
    t.integer  "horo_id",    limit: 4
    t.date     "date"
    t.float    "star",       limit: 24
    t.text     "content",    limit: 65535
    t.string   "gender",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "horo_daily_entries", ["horo_id"], name: "index_horo_daily_entries_on_horo_id", using: :btree

  create_table "horo_daily_entry_translations", force: :cascade do |t|
    t.integer  "horo_daily_entry_id", limit: 4,     null: false
    t.string   "locale",              limit: 255,   null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.text     "content",             limit: 65535
  end

  add_index "horo_daily_entry_translations", ["horo_daily_entry_id"], name: "index_horo_daily_entry_translations_on_horo_daily_entry_id", using: :btree
  add_index "horo_daily_entry_translations", ["locale"], name: "index_horo_daily_entry_translations_on_locale", using: :btree

  create_table "horo_monthly_entries", force: :cascade do |t|
    t.integer  "horo_id",    limit: 4
    t.integer  "month",      limit: 4
    t.float    "star",       limit: 24
    t.text     "content",    limit: 65535
    t.string   "gender",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "horo_monthly_entries", ["horo_id"], name: "index_horo_monthly_entries_on_horo_id", using: :btree

  create_table "horo_monthly_entry_translations", force: :cascade do |t|
    t.integer  "horo_monthly_entry_id", limit: 4,     null: false
    t.string   "locale",                limit: 255,   null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.text     "content",               limit: 65535
  end

  add_index "horo_monthly_entry_translations", ["horo_monthly_entry_id"], name: "index_horo_monthly_entry_translations_on_horo_monthly_entry_id", using: :btree
  add_index "horo_monthly_entry_translations", ["locale"], name: "index_horo_monthly_entry_translations_on_locale", using: :btree

  create_table "horo_profile_translations", force: :cascade do |t|
    t.integer  "horo_profile_id", limit: 4,     null: false
    t.string   "locale",          limit: 255,   null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.text     "description",     limit: 65535
    t.text     "content",         limit: 65535
  end

  add_index "horo_profile_translations", ["horo_profile_id"], name: "index_horo_profile_translations_on_horo_profile_id", using: :btree
  add_index "horo_profile_translations", ["locale"], name: "index_horo_profile_translations_on_locale", using: :btree

  create_table "horo_profiles", force: :cascade do |t|
    t.integer  "horo_id",     limit: 4
    t.text     "description", limit: 65535
    t.text     "content",     limit: 65535
    t.string   "gender",      limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "horo_profiles", ["horo_id"], name: "index_horo_profiles_on_horo_id", using: :btree

  create_table "horo_translations", force: :cascade do |t|
    t.integer  "horo_id",    limit: 4,     null: false
    t.string   "locale",     limit: 255,   null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.text     "name",       limit: 65535
  end

  add_index "horo_translations", ["horo_id"], name: "index_horo_translations_on_horo_id", using: :btree
  add_index "horo_translations", ["locale"], name: "index_horo_translations_on_locale", using: :btree

  create_table "horo_weekly_entries", force: :cascade do |t|
    t.integer  "horo_id",    limit: 4
    t.date     "from_date"
    t.date     "to_date"
    t.float    "star",       limit: 24
    t.text     "content",    limit: 65535
    t.string   "gender",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "horo_weekly_entries", ["horo_id"], name: "index_horo_weekly_entries_on_horo_id", using: :btree

  create_table "horo_weekly_entry_translations", force: :cascade do |t|
    t.integer  "horo_weekly_entry_id", limit: 4,     null: false
    t.string   "locale",               limit: 255,   null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.text     "content",              limit: 65535
  end

  add_index "horo_weekly_entry_translations", ["horo_weekly_entry_id"], name: "index_horo_weekly_entry_translations_on_horo_weekly_entry_id", using: :btree
  add_index "horo_weekly_entry_translations", ["locale"], name: "index_horo_weekly_entry_translations_on_locale", using: :btree

  create_table "horo_yearly_entries", force: :cascade do |t|
    t.integer  "horo_id",    limit: 4
    t.integer  "year",       limit: 4
    t.float    "star",       limit: 24
    t.text     "content",    limit: 65535
    t.string   "gender",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "horo_yearly_entries", ["horo_id"], name: "index_horo_yearly_entries_on_horo_id", using: :btree

  create_table "horo_yearly_entry_translations", force: :cascade do |t|
    t.integer  "horo_yearly_entry_id", limit: 4,     null: false
    t.string   "locale",               limit: 255,   null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.text     "content",              limit: 65535
  end

  add_index "horo_yearly_entry_translations", ["horo_yearly_entry_id"], name: "index_horo_yearly_entry_translations_on_horo_yearly_entry_id", using: :btree
  add_index "horo_yearly_entry_translations", ["locale"], name: "index_horo_yearly_entry_translations_on_locale", using: :btree

  create_table "horos", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "code",       limit: 255
    t.string   "sign",       limit: 255
    t.integer  "from_date",  limit: 4
    t.integer  "from_month", limit: 4
    t.integer  "to_date",    limit: 4
    t.integer  "to_month",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "email",           limit: 255
    t.string   "fb_id",           limit: 255
    t.string   "password_digest", limit: 255
    t.date     "dob"
    t.time     "tob"
    t.string   "timezone",        limit: 255
    t.string   "gender",          limit: 255
    t.string   "avatar",          limit: 255
    t.string   "token",           limit: 255
    t.datetime "last_login"
    t.integer  "horo_id",         limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "users", ["horo_id"], name: "index_users_on_horo_id", using: :btree

  add_foreign_key "compatibilities", "horos", column: "first_horo"
  add_foreign_key "compatibilities", "horos", column: "second_horo"
  add_foreign_key "horo_daily_entries", "horos"
  add_foreign_key "horo_monthly_entries", "horos"
  add_foreign_key "horo_profiles", "horos"
  add_foreign_key "horo_weekly_entries", "horos"
  add_foreign_key "horo_yearly_entries", "horos"
  add_foreign_key "users", "horos"
end
