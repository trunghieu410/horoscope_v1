puts "Horo"
file_to_load  = Rails.root + 'db/seed/horos.yml'
horos_list   = YAML::load( File.open( file_to_load ) )
horos_list.each_pair do |key,u|
  s = Horo.find_by_name(u['name'])
  unless s
    c = Horo.create(u)
  else
  	s.update_attributes(:code          => u['code'], 
                        :from_date     => u['from_date'], 
                        :from_month    => u['from_month'], 
                        :to_date       => u['to_date'], 
                        :to_month      => u['to_month'])
  end
end


puts "Horoscope Profiles"
file_to_load  = Rails.root + 'db/seed/horos_profiles.yml'
horosprofile_list   = YAML::load( File.open( file_to_load ) )
horosprofile_list.each_pair do |key,u|
  s = HoroProfile.where(["horo_id = ? and gender = ?",  u['horo_id'], u['gender']]).first
  unless s
    c = HoroProfile.create(u)
  else
    s.update_attributes(:description       => u['description'], 
                        :content           => u['content'],
                        :gender            => u['gender'])
  end
end

puts "Admin"
file_to_load  = Rails.root + 'db/seed/admin.yml'
admin   = YAML::load( File.open( file_to_load ) )
admin.each_pair do |key,u|
  s = Admin.find_by_email(u['email'])
  unless s
    c = Admin.create(u)
  end
end

puts "User"
file_to_load  = Rails.root + 'db/seed/user.yml'
user   = YAML::load( File.open( file_to_load ) )
user.each_pair do |key,u|
  s = User.find_by_email(u['email'])
  unless s
    c = User.create(u)
  end
end