class CreateHoroMonthlyEntries < ActiveRecord::Migration
  def change
    create_table :horo_monthly_entries do |t|
      t.references :horo, index: true, foreign_key: true
      t.integer :month
      t.float :star
      t.text :content
      t.string :gender

      t.timestamps null: false
    end
  end
end
