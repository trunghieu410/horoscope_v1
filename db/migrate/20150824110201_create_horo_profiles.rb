class CreateHoroProfiles < ActiveRecord::Migration
  def change
    create_table :horo_profiles do |t|
      t.references :horo, index: true, foreign_key: true
      t.text :description
      t.text :content
      t.string :gender

      t.timestamps null: false
    end
  end
end
