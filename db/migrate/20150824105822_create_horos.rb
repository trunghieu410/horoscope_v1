class CreateHoros < ActiveRecord::Migration
  def change
    create_table :horos do |t|
      t.string :name
      t.string :code
      t.string :sign
      t.integer :from_date
      t.integer :from_month
      t.integer :to_date
      t.integer :to_month

      t.timestamps null: false
    end
  end
end
