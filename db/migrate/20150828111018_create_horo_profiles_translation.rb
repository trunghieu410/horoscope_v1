class CreateHoroProfilesTranslation < ActiveRecord::Migration
  def up
    HoroProfile.create_translation_table!({
      description: :text,
      content: :text
    }, {
      migrate_data: true
    })
  end

  def down
    HoroProfile.drop_translation_table! migrate_data: true
  end
end
