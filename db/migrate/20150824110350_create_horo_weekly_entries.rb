class CreateHoroWeeklyEntries < ActiveRecord::Migration
  def change
    create_table :horo_weekly_entries do |t|
      t.references :horo, index: true, foreign_key: true
      t.date :from_date
      t.date :to_date
      t.float :star
      t.text :content
      t.string :gender

      t.timestamps null: false
    end
  end
end
