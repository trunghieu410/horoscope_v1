class CreateCompatibilitesTranslation < ActiveRecord::Migration
  def up
    Compatibility.create_translation_table!({
      description: :text,
      content: :text
    }, {
      migrate_data: true
    })
  end

  def down
    Compatibility.drop_translation_table! migrate_data: true
  end
end
