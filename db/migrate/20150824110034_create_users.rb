class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :fb_id
      t.string :password_digest
      t.date :dob
      t.time :tob
      t.string :timezone
      t.string :gender
      t.string :avatar
      t.string :token
      t.datetime :last_login
      t.references :horo, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
