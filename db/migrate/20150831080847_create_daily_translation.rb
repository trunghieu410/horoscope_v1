class CreateDailyTranslation < ActiveRecord::Migration
  def up
    HoroDailyEntry.create_translation_table!({
      content: :text
    }, {
      migrate_data: true
    })
  end

  def down
    HoroDailyEntry.drop_translation_table! migrate_data: true
  end
end
