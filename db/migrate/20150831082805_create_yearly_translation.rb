class CreateYearlyTranslation < ActiveRecord::Migration
  def up
    HoroYearlyEntry.create_translation_table!({
      content: :text
    }, {
      migrate_data: true
    })
  end

  def down
    HoroYearlyEntry.drop_translation_table! migrate_data: true
  end
end
