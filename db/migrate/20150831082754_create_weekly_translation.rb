class CreateWeeklyTranslation < ActiveRecord::Migration
  def up
    HoroWeeklyEntry.create_translation_table!({
      content: :text
    }, {
      migrate_data: true
    })
  end

  def down
    HoroWeeklyEntry.drop_translation_table! migrate_data: true
  end
end
