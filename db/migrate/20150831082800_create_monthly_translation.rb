class CreateMonthlyTranslation < ActiveRecord::Migration
  def up
    HoroMonthlyEntry.create_translation_table!({
      content: :text
    }, {
      migrate_data: true
    })
  end

  def down
    HoroMonthlyEntry.drop_translation_table! migrate_data: true
  end
end
