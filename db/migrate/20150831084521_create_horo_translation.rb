class CreateHoroTranslation < ActiveRecord::Migration
   def up
    Horo.create_translation_table!({
      name: :text
    }, {
      migrate_data: true
    })
  end

  def down
    Horo.drop_translation_table! migrate_data: true
  end
end
