class CreateHoroYearlyEntries < ActiveRecord::Migration
  def change
    create_table :horo_yearly_entries do |t|
      t.references :horo, index: true, foreign_key: true
      t.integer :year
      t.float :star
      t.text :content
      t.string :gender

      t.timestamps null: false
    end
  end
end
