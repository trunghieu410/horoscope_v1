class CreateCompatibilities < ActiveRecord::Migration
  def change
    create_table :compatibilities do |t|
      t.integer :first_horo, :references => "horo", index: true, foreign_key: true
      t.integer :second_horo, :references => "horo", index: true, foreign_key: true
      t.string :gender
      t.float :star
      t.text :description
      t.text :content

      t.timestamps null: false
    end
    add_foreign_key :compatibilities, :horos, column: :first_horo
    add_foreign_key :compatibilities, :horos, column: :second_horo
  end
end

